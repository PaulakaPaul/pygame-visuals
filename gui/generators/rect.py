import random

import gui.settings as s
from gui.generators.base_classes import BaseGenerator
from gui.drawers import DefaultRectDrawer, RectDrawer


class RectCenterGenerator(BaseGenerator):
    X_MAX_POINT = s.WINSIZE[0]
    Y_MAX_POINT = s.WINSIZE[1]

    X_MIN_POINT = X_MAX_POINT / 2
    Y_MIN_POINT = Y_MAX_POINT / 2

    def __init__(self, screen, intensity):
        assert intensity <= 1  # It will be used like a probability.

        super().__init__(screen, intensity)
        self.positions = []
        self.drawer = DefaultRectDrawer(self.screen)

    def generate(self):
        for pos in self._generate_points():
            self._draw_if_condition_passes(pos)
            self.positions.append(pos)

    def _generate_points(self):
        x_starting_point = self._compute_starting_point(self.X_MAX_POINT, self.X_MIN_POINT)
        y_starting_point = self._compute_starting_point(self.Y_MAX_POINT, self.Y_MIN_POINT)

        x_ending_point = self._compute_ending_point(self.X_MAX_POINT, x_starting_point)
        y_ending_point = self._compute_ending_point(self.Y_MAX_POINT, y_starting_point)

        x_point = x_starting_point
        while x_point >= x_ending_point:
            y_point = y_starting_point
            while y_point >= y_ending_point:
                yield x_point, y_point
                y_point -= s.SQUARE_LAT

            x_point -= s.SQUARE_LAT

    def _compute_starting_point(self, max_point, min_point):
        range_to_be_calibrated = max_point - min_point
        calibrated_range = range_to_be_calibrated * self.intensity

        actual_square_range = min_point + calibrated_range

        return actual_square_range

    def _compute_ending_point(self, max_point, starting_point):
        border_rect_difference = max_point - starting_point

        return border_rect_difference

    def _draw_if_condition_passes(self, pos):
        if self._should_draw():
            self.drawer.draw(pos)

    def _should_draw(self):
        return random.uniform(0, 1) > self.intensity

    def clear(self):
        for position in self.positions:
            self.drawer.erase(position)

        self.positions.clear()


class RectPointsGenerator(BaseGenerator):
    def __init__(self, screen, intensity, rects):
        assert intensity <= 1  # It will be used like a probability.

        super().__init__(screen, intensity)
        self.rects = rects
        self.drawer = RectDrawer(self.screen)

    def generate(self):
        for rect in self.rects:
            self.drawer.draw(rect)

    def clear(self):
        for rect in self.rects:
            self.drawer.erase(rect)
