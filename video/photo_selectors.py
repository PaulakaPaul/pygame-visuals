import cv2
import video.settings as s


class PhotoRecognizer:
    def get_rects(self, photo):
        raise NotImplementedError()


class PhotoSelectiveSearchSegmentation(PhotoRecognizer):
    def __init__(self):
        cv2.setUseOptimized(True)
        cv2.setNumThreads(8)

        self.ss = cv2.ximgproc.segmentation.createSelectiveSearchSegmentation()

    def get_rects(self, photo):
        self.ss.setBaseImage(photo)
        self.ss.switchToSelectiveSearchFast()

        rects = self.ss.process()
        return self._filter_rects(rects)

    def _filter_rects(self, rects):
        for i, rect in enumerate(rects):
            if i < s.MAX_RECT_NUMBER:
                if self._should_keep(rect):  # x, y, w, h = rect
                    yield rect
            else:
                break

    def _should_keep(self, rect):
        x, y, w, h = rect

        return w <= s.MAX_RECT_WIDTH and h <= s.MAX_RECT_HEIGHT


