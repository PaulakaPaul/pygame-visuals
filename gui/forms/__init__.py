import gui.settings as s

from gui.forms.geometric_forms import *


class AbstractFormFactory:
    @classmethod
    def get_square(cls, pos):
        raise NotImplementedError()

    @classmethod
    def get_rect(cls, pos, width, height):
        raise NotImplementedError()


class RedFormFactory(AbstractFormFactory):
    FACTORY_COLOUR = s.Colours.RED

    SQUARE_LAT = s.SQUARE_LAT

    @classmethod
    def get_square(cls, pos):
        return Square(cls.SQUARE_LAT, pos, cls.FACTORY_COLOUR)

    @classmethod
    def get_rect(cls, pos, width, height):
        return Rect(width, height, pos, cls.FACTORY_COLOUR)
