from gui.forms import RedFormFactory
from gui.settings import WINSIZE
from video.parameter_getters import *
from video.settings import MAX_RECT_WIDTH


class RectAdaptor:
    """
    Adapt rects from the photo_selectors to pygame gui rects.

        * photo_selector rect structure: x, y, w, h = rect
    """
    pygame_rect_factory_method = None

    def __init__(self, selector_rects):
        self.pygame_rects = self._adapt(selector_rects)

    def get_adapted_rects(self):
        return self.pygame_rects

    def _adapt(self, selector_rects):
        assert self.pygame_rect_factory_method is not None

        adapted_rects = []
        for rect in selector_rects:
            x, y, w, h = rect

            adapted_x = self._translate_point(x, PHOTO_WIDTH, WINSIZE[0])
            adapted_y = self._translate_point(y, PHOTO_HEIGHT, WINSIZE[1])

            adapted_rect = self.pygame_rect_factory_method((adapted_x, adapted_y), w, h)
            adapted_rects.append(adapted_rect)

        return adapted_rects

    def _translate_point(self, point, max_video_value, max_gui_value):
        return point * max_gui_value / max_video_value


class RedRectAdaptor(RectAdaptor):
    pygame_rect_factory_method = RedFormFactory.get_rect
