class BaseForm:
    def __init__(self, width, height, pos, colour):
        assert len(pos) == 2
        assert pos[0] >= 0
        assert pos[1] >= 0

        assert width > 0
        assert height > 0

        self.pos = pos
        self.width = width
        self.height = height
        self.colour = colour

    def get_form_or_create(self):
        raise NotImplementedError()

    def get_colour(self):
        return self.colour
