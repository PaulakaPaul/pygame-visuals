import pygame
from pygame.locals import *

import gui.settings as gui_settings
from gui.generators.rect import RectPointsGenerator
from audio import AudioRMSGetter
from utils.translators import DivideTranslator
from video import VideoCameraGetter, RedRectAdaptor
from video.photo_selectors import PhotoSelectiveSearchSegmentation


def loop():
    clock = pygame.time.Clock()

    # initialize and prepare screen
    pygame.init()
    screen = pygame.display.set_mode(gui_settings.WINSIZE)
    pygame.display.set_caption(gui_settings.TITLE)
    screen.fill(gui_settings.BACKGROUND_COLOUR)

    done = 0

    audio_rms_getter = AudioRMSGetter()
    video_camera_getter = VideoCameraGetter()
    photo_recognizer = PhotoSelectiveSearchSegmentation()

    prob_translator = DivideTranslator()

    old_generator = None

    while not done:
        # Delete the old gui.
        if old_generator:
            old_generator.clear()

        # intensity = audio_rms_getter.get_parameter()
        intensity = 0.5
        prob_intensity = prob_translator.to_prob(intensity)

        frame = video_camera_getter.get_parameter()
        selector_rects = photo_recognizer.get_rects(frame)
        gui_rects = RedRectAdaptor(selector_rects).get_adapted_rects()

        # Create the new gui.
        generator = RectPointsGenerator(screen, prob_intensity, gui_rects)
        generator.generate()
        old_generator = generator

        pygame.display.update()

        # pygame.time.wait(main_settings.WAIT_TIME)

        for e in pygame.event.get():
            if e.type == QUIT or (e.type == KEYUP and e.key == K_ESCAPE):
                done = 1
                break

        clock.tick(50)

    audio_rms_getter.stop()
    video_camera_getter.close()


if __name__ == '__main__':
    loop()


