import pygame

from gui.forms.base_classes import BaseForm


class Rect(BaseForm):
    def __init__(self, width, height, pos, colour):
        super().__init__(width, height, pos, colour)

        self.rect = None

    def get_form_or_create(self):
        if self.rect is None:
            self.rect = pygame.Rect(self.pos[0], self.pos[1], self.width, self.height)

        return self.rect


class Square(Rect):
    def __init__(self, lat, pos, colour):
        super().__init__(lat, lat, pos, colour)
