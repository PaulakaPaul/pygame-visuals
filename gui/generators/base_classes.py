class BaseGenerator:
    def __init__(self, screen, intensity):
        self.screen = screen
        self.intensity = intensity

    def generate(self):
        raise NotImplementedError()

    def clear(self):
        raise NotImplementedError()
