import pygame

import gui.settings as s


class Unit():
    def __init__(self):
        self.last = pygame.time.get_ticks()
        self.cooldown = 300

    def fire(self):
        now = pygame.time.get_ticks()

        while now - self.last < self.cooldown:
            now = pygame.time.get_ticks()

        return True


class Blinker:
    draw_handler = None
    DELAY_MILLISECONDS = 1000

    def __init__(self, screen, form):
        self.screen = screen
        self.form = form

    def blink(self):
        assert self.draw_handler is not None

        self.draw_handler(self.screen, s.BACKGROUND_COLOUR, self.form.get_form_or_create())
        pygame.display.update()

        Unit().fire()

        self.draw_handler(self.screen, self.form.get_colour(), self.form.get_form_or_create())
        pygame.display.update()


class SquareBlinker(Blinker):
    draw_handler = pygame.draw.rect
