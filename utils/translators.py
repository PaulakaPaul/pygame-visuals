class Translator:
    def to_prob(self, value):
        raise NotImplementedError()


class DivideTranslator(Translator):
    def __init__(self):
        self.max_value = 1

    def to_prob(self, value):
        if value < 0.:
            value *= -1.

        if value <= 1.:
            return value  # Already a probability.

        if value > self.max_value:
            self.max_value = value  # Always save the max value to be able to map the value in the [0,1] interval

        return value / self.max_value
