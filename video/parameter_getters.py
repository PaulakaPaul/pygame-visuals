import cv2

from video.settings import PHOTO_HEIGHT, PHOTO_WIDTH


class VideoParameterGetter:
    def get_parameter(self):
        raise NotImplementedError()

    def open(self):
        raise NotImplementedError()

    def close(self):
        raise NotImplementedError()

    def _resize_frame(self, frame):
        return cv2.resize(frame, (PHOTO_WIDTH, PHOTO_HEIGHT))


class VideoCameraGetter(VideoParameterGetter):
    def __init__(self):
        self.open()

    def get_parameter(self):
        _, frame = self.video_capture.read()

        if frame is None:
            return None

        return self._resize_frame(frame)

    def open(self):
        self.video_capture = cv2.VideoCapture(0)

    def close(self):
        self.video_capture.release()
