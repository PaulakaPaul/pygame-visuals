import pygame


import gui.settings as s
from gui.forms import RedFormFactory


class Drawer:
    draw_handler = None

    def __init__(self, screen):
        self.screen = screen

    def draw(self, form):
        assert self.draw_handler is not None

        self.draw_handler(self.screen, form.get_colour(), form.get_form_or_create())

    def erase(self, form):
        assert self.draw_handler is not None

        self.draw_handler(self.screen, s.BACKGROUND_COLOUR, form.get_form_or_create())


class DefaultFormDrawer(Drawer):
    form_factory_handler = None

    def draw(self, pos):
        assert self.form_factory_handler is not None

        form = self.form_factory_handler(pos)
        super().draw(form)

    def erase(self, pos):
        assert self.form_factory_handler is not None

        form = self.form_factory_handler(pos)
        super().erase(form)


class RectDrawer(Drawer):
    draw_handler = pygame.draw.rect


class DefaultRectDrawer(DefaultFormDrawer):
    draw_handler = pygame.draw.rect
    form_factory_handler = RedFormFactory.get_square
