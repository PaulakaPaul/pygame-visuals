# General pygame setup
TITLE = 'Visuals'

WINSIZE = [780, 620]
WINCENTER_DEFAULT = [320, 240]

SQUARE_LAT = 10


class Colours:
    WHITE = 255, 240, 200
    BLACK = 20, 20, 40

    RED = 255, 40, 40


BACKGROUND_COLOUR = Colours.BLACK
